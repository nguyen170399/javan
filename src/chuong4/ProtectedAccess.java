package chuong4;

public class ProtectedAccess {
	  protected String text = "floating";          // protected access
	  protected void floatInWater() {               // protected access
	    System.out.println(text);
	  } }
	