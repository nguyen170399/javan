package chuong4;

public class StaticVsIn {
	  private static String name = "Static class";
	  public static void first() {  }
	  public static void second() {  }
	 // public void third() {  System.out.println(name); }
	  public static void third() {  System.out.println(name); }//loi khi khong co static o name
	  public static void main(String args[]) {
	    first();
	    second();
	    third();          // DOES NOT COMPILE khi khong co static
	  } }