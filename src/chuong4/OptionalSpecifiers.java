package chuong4;

public class OptionalSpecifiers {
	public void walk1() {}
	public void walk2() { return; }
	public String walk3() { return ""; }
	public final static void walk4() {}
	//public modifier void walk5() {} // DOES NOT COMPILE
	//public void final walk6() {} // DOES NOT COMPILE
	final public void walk7() {}
}
	