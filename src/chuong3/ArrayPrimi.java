package chuong3;

public class ArrayPrimi {
	public static void main (String arg[]){
		 String [] bugs = { "cricket", "beetle", "ladybug" };
		    String [] alias = bugs;
		    System.out.println(bugs.equals(alias));     // true
		    System.out.println(bugs.toString());
		    System.out.println("---------");
		    String[] mammals = {"monkey", "chimp", "donkey"};
		    System.out.println(mammals.length);           // 3
		    System.out.println(mammals[0]);               // monkey
		    System.out.println(mammals[1]);               // chimp
		    System.out.println(mammals[2]);
		    System.out.println("---------");
		    String[] birds = new String[6];
		    System.out.println(birds.length);
		    System.out.println("---------");
		    int[] numbers = new int[10];
		    for (int i = 0; i < numbers.length; i++){ 
		    	numbers[i] = i + 5;
		    	System.out.print(numbers[i]+"\t");
		    }
		   
	}
}
