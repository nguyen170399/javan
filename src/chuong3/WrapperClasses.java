package chuong3;

import java.util.ArrayList;

public class WrapperClasses {
	public static void main (String arg[]){
		ArrayList<Double> weights = new ArrayList();
		weights.add(50.5);               // [50.5]
		weights.add(new Double(60));     // [50.5, 60.0]
		weights.remove(50.5);               // [60.0]
		double first = weights.get(0);     // 60.0
		System.out.println(weights);
		System.out.println(first);
		System.out.println("-------------");
		ArrayList<Integer> heights = new ArrayList();
		heights.add(null);
		//int h = heights.get(0); //ko xac dinh
		System.out.println(heights);
		//System.out.println(h);
		System.out.println("-------------");
		ArrayList<Integer> numbers = new ArrayList();
		numbers.add(1);
		numbers.add(2);
		numbers.remove(1);
		System.out.println(numbers);
	}
}
