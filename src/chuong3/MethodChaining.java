package chuong3;

public class MethodChaining {
	public static void main(String arg[]){
		String start = "AniMaL   ";//AniMaL
		//String trimmed = start.trim();
		//String lowercase = trimmed.toLowerCase();      // "animal"
	//	String result = lowercase.replace('a', 'A');   // "Animal"
		//System.out.println(result);
		String result = start.trim().toLowerCase().replace('a', 'A');
		System.out.println(result);
		String a = "abc";
		String b = a.toUpperCase();
		System.out.println("a=" + a);
		System.out.println("b=" + b);
	}
}
