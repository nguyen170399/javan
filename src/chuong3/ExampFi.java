package chuong3;

public class ExampFi {
	public static void main(String[] args) {
		int numFish = 4;
		String fishType = "tuna";
		//String anotherFish = numFish + 1; change anotherFish to int
		System.out.println(" " + fishType);
		System.out.println(numFish + " " + 1);
		System.out.println("---------------------");
		String numbers = "012345678";
		System.out.println(numbers.substring(1, 3));
		System.out.println(numbers.substring(7, 7));
		System.out.println(numbers.substring(7));
	}
}											
