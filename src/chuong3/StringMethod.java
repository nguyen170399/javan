package chuong3;

public class StringMethod {
	public static void main (String arg[]){
		String name = "Fluffy";
		System.out.println("length() :"+name.length());
		System.out.println("charAt(0) v� charAt(5) :"+name.charAt(0)+" "+name.charAt(5));
		System.out.println("indexOf :"+name.indexOf('f')+" "+name.indexOf("lu",5)+" "+name.indexOf("lu"));
		System.out.println("substring() :"+name.substring(2)+" "+name.substring(3,5)+" "+name.substring(3,4));
		System.out.println("toUppercase :"+name.toUpperCase());	
		System.out.println("equals :"+name.equals("FLUFY"));
	}		
}
	