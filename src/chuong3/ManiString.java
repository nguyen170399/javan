package chuong3;

public class ManiString {
	public static void main (String arg[]){
		String name1 = "Fluffy";
		String name2 = new String("Fluffy");
		String name3= name1;
		System.out.println(name1);
		System.out.println(name2);
		if (name1==name2) {
			System.out.println("name 1 = name 2");
		}
		if (name3==name2) {
			System.out.println("name 2 = name 3");
		}
		if (name1==name3) {
			System.out.println("name 3 = name 1");
		}
		System.out.println("------");
		System.out.println(1+2);
		System.out.println("1"+"2");
		System.out.println(1>=2);
		System.out.println("------");
		String s1 = "1";
		String s2 = s1.concat("2");
		s2.concat("3");
		System.out.println(s2);
	}
}
