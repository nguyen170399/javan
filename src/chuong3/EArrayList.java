package chuong3;

import java.util.ArrayList;
import java.util.List;

public class EArrayList {
	public static void main (String arg[]){
		//ArrayList list1 = new ArrayList();
		//ArrayList list2 = new ArrayList(10);
		//ArrayList list3 = new ArrayList(list2);
		ArrayList list = new ArrayList();
		list.add("hawk");          // [hawk]
		list.add(Boolean.TRUE);    // [hawk, true]
		list.add("only you");
		System.out.println(list);  // [hawk, true]
		ArrayList list3 = new ArrayList(list);
		list.add("i don't care");
		System.out.println(list3);
		System.out.println(list);
		System.out.println("------------------");
		ArrayList<String> birds = new ArrayList();
		birds.add("hawk");            // [hawk]
		birds.add(1, "robin");        // [hawk, robin]
		birds.add(0, "blue jay");     // [blue jay, hawk, robin]
		birds.add(1, "cardinal");     // [blue jay, cardinal, hawk, robin]
		System.out.println(birds);    // [blue jay, cardinal, hawk, robin]
		System.out.println("------------------");
		List<String> list2 = new ArrayList();
		list2.add("hawk");     // [hawk]
		list2.add("hawk");     // [hawk, hawk]
		System.out.println(list2.remove("cardinal")); // prints false
		System.out.println(list2.remove("hawk")); // prints true
		System.out.println(list2.remove(0)); // prints hawk
		System.out.println(list2);     // []
	}
}
