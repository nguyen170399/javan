package chuong2;

public class NestedLoops {
	public static void main(String arg[]){
		//int[][] myComplexArray = {{5,2,1,3},{3,9,8,9},{5,7,12,7}};
		//for(int[] mySimpleArray : myComplexArray) {
		 // for(int i=0; i<mySimpleArray.length; i++) {
		 //   System.out.print(mySimpleArray[i]+"\t");
		//  }
		//  System.out.println();
		//}
		//System.out.println("---------");
		//int x = 20;
		//while(x>0) {
		 // do {
		//    x -= 2;
		//  } while (x>5);
		 // x--;
		//  System.out.print(x+"\t");
		//}
		 int[][] list = {{1,13,5},{1,2,5},{2,7,2}};
		    int searchValue = 2;
		    int positionX = -1;
		    int positionY = -1;
		   for(int i=0; i<3;i++){
			   for (int j = 0; j < 3; j++) {
				System.out.println(list[i][j]);
			   }
			   System.out.println("\n");
		   }
		    PARENT_LOOP: for(int i=0; i<list.length; i++) {
		      for(int j=0; j<list[i].length; j++) {
		        if(list[i][j]==searchValue) {
		          positionX = i;
		          positionY = j;
		          System.out.println(list[i][j]+"\t");
		          break PARENT_LOOP;
		        }
		      }
		    }
		    if(positionX==-1 || positionY==-1) {
		      System.out.println("Value "+searchValue+" not found");
		    } else {
		      System.out.println("Value "+searchValue+" found at: " +
		        "("+positionX+","+positionY+")");
		    }

	}
}
