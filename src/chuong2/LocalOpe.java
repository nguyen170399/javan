package chuong2;

public class LocalOpe {
	public static void main (String arg[]){
		boolean x=true;
		boolean y=false;
		System.out.println("Quan he & dung khi:");
		for(int i=0;i<4; i++){
			if(i==0){
				x=true;
				y=true;
				if(x&y){
					System.out.println(x+" & "+y);
				}
			}
			if(i==0){
				x=true;
				y=false;
				if(x&y){
					System.out.println(x+" & "+y);
				}
			}
			if(i==0){
				x=false;
				y=true;
				if(x&y){
					System.out.println(x+" & "+y);
				}
			}
			if(i==4){
				x=false;
				y=false;
				if(x&y){
					System.out.println(x+" & "+y);
				}
			}
		}
		System.out.println("---------------------------");
		System.out.println("Quan he | dung khi :");
		for(int i=0;i<4; i++){
			if(i==0){
				x=true;
				y=true;
				if(x|y){
					System.out.println(x+" | "+y);
				}
			}
			if(i==0){
				x=true;
				y=false;
				if(x|y){
					System.out.println(x+" | "+y);
				}
			}
			if(i==0){
				x=false;
				y=true;
				if(x|y){
					System.out.println(x+" | "+y);
				}
			}
			if(i==4){
				x=false;
				y=false;
				if(x|y){
					System.out.println(x+" | "+y);
				}
			}
		}
		System.out.println("---------------------------");
		System.out.println("Quan he ^ dung khi :");
		for(int i=0;i<4; i++){
			if(i==0){
				x=true;
				y=true;
				if(x^y){
					System.out.println(x+" ^ "+y);
				}
			}
			if(i==0){
				x=true;
				y=false;
				if(x^y){
					System.out.println(x+" ^ "+y);
				}
			}
			if(i==0){
				x=false;
				y=true;
				if(x^y){
					System.out.println(x+" ^ "+y);
				}
			}
			if(i==4){
				x=false;
				y=false;
				if(x^y){
					System.out.println(x+" ^ "+y);
				}
			}
		}
	}
	
}
