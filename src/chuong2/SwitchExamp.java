package chuong2;

public class SwitchExamp {
	public static void main (String arg []){
		int a=10;
		switch(a){
			case 5: 
				System.out.println("5");
			case 10:
				System.out.println("10");
				break;
			
			case 11:
				System.out.println("11");
			case 12:
				System.out.println("12");
		}
		System.out.println("a = "+a);
		//int dayOfWeek = 5;
		//switch(dayOfWeek) {
			//default:
				//System.out.println("Weekday");
				
			//case 5:
				//System.out.println("Sunday");
				
			//case 2:
			//	System.out.println("Saturday");
				
		//}
	}
}
