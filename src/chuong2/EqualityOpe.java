package chuong2;

import java.io.File;

public class EqualityOpe {
	public static void main(String arg[]){
	//	boolean x = true == 3;  sai
	//	boolean y = false != "Giraffe"; sai
	//	boolean z = 3 == "Kangaroo"; sai
		boolean y = false;
		boolean x = (y = true);
		System.out.println("y = "+y);
		System.out.println("x = "+x); 
		System.out.println("------------");
		File a = new File("myFile.txt");
		File b = new File("myFile.txt");
		File c = a;
		System.out.println(a == b);  // Outputs false
		System.out.println(a == c);  // Outputs true
	}
}
